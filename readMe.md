# Quote pad test application #

This is spring + react js test application.

## Database initialization ##

create database DZ_GRASSHOPPER;

create user dz_user with password 'asdf';

GRANT ALL privileges ON DATABASE DZ_GRASSHOPPER TO dz_user;

## API usage ##

### Add quote ###
curl --data '{"author":"A2", "quote":"quote 1"}' http://localhost:8080/quotes/add -H "Content-Type: application/json"

{"result":"SUCCESS"}

### Get authors list ###
curl http://localhost:8080/authors/load

{"result":"SUCCESS", "items":[{"name":"Author", "value":"Author"}]}

### Get available sort types ###
curl http://localhost:8080/sortTypes/load

{"result":"SUCCESS", "items":[{"value":"DATE_DESC","name":"По дате"},{"value":"DATE_ASC","name":"По дате. Старые в начале"},{"value":"AUTHOR","name":"По автору"}]}


## Run application ##
./gradlew bootRun