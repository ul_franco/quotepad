package ru.dz.grasshopper.controller.response.quotes;

import org.junit.Test;
import ru.dz.grasshopper.model.SortType;
import ru.dz.grasshopper.model.entity.Author;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Created by grasshopper on 23.02.16.
 */
public class SelectItemResponseTest {

    @Test
    public void testOf() {
        List<Author> authorList = Arrays.asList(new Author("A1"), new Author("A2"));
        SelectItemResponse response = SelectItemResponse.of(authorList);
        assertNotNull(response.getItems());
        assertEquals(2, response.getItems().size());

        SelectItem si = response.getItems().get(0);
        assertEquals("A1", si.getName());
        assertEquals("A1", si.getValue());
    }

    @Test
    public void testOfSortType() {
        SelectItemResponse response = SelectItemResponse.ofSortType();
        assertNotNull(response.getItems());
        assertEquals(SortType.values().length, response.getItems().size());

        SelectItem si = response.getItems().get(0);
        assertEquals(SortType.DATE_DESC.getDisplayName(), si.getName());
        assertEquals(SortType.DATE_DESC.name(), si.getValue());
    }
}