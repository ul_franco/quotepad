package ru.dz.grasshopper.controller.response.quotes;

import org.junit.Test;
import ru.dz.grasshopper.model.SortType;
import ru.dz.grasshopper.model.entity.Author;

import static org.junit.Assert.assertEquals;

/**
 * Created by grasshopper on 19.02.16.
 */
public class SelectItemTest {

    @Test
    public void test() {
        String authorName = "Ivan";
        SelectItem si = SelectItem.builder().author(new Author(authorName)).build();
        assertEquals(authorName, si.getName());
        assertEquals(authorName, si.getValue());

        si = SelectItem.builder().sortType(SortType.DATE_ASC).build();
        assertEquals(SortType.DATE_ASC.getDisplayName(), si.getName());
        assertEquals(SortType.DATE_ASC.name(), si.getValue());
    }
}