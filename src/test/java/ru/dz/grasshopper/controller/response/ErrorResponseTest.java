package ru.dz.grasshopper.controller.response;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by olegmakeev on 15.02.16.
 */
public class ErrorResponseTest {

    @Test
    public void testOf() {
        ErrorResponse er = ErrorResponse.of(ErrorCode.BAD_REQUEST);
        assertEquals(ErrorCode.BAD_REQUEST.getCode(), er.getCode());
        assertEquals(ErrorCode.BAD_REQUEST.getDescription(), er.getDescription());

        er = ErrorResponse.of(ErrorCode.INTERNAL_ERROR, "Custom description");
        assertEquals(ErrorCode.INTERNAL_ERROR.getCode(), er.getCode());
        assertEquals("Custom description", er.getDescription());
    }

}