package ru.dz.grasshopper.controller;

import com.google.gson.Gson;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import ru.dz.grasshopper.config.JpaTestConfig;
import ru.dz.grasshopper.controller.response.quotes.SelectItemResponse;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Created by grasshopper on 23.02.16.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {JpaTestConfig.class, WebAppContext.class})
@WebAppConfiguration
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class QuotesControllerTest {

    private Gson gson = new Gson();
    private MockMvc mockMvc;

    @Autowired
    private QuotesController controller;


    @Before
    public void setUp() {
        mockMvc = MockMvcBuilders.standaloneSetup(controller).build();
    }

    @Test
    public void test1AddQuote() throws Exception {

        MockHttpServletRequestBuilder requestBuilder = post("/quotes/add").
                content("{\"quote\":\"quote 1\", \"author\":\"Author\"}").
                contentType(MediaType.APPLICATION_JSON_UTF8);
        mockMvc.perform(requestBuilder)
                .andExpect(status().isOk()).
                andExpect(content().json("{\"result\":\"SUCCESS\"}"));

        requestBuilder = post("/quotes/add").
                content("{\"author\":\"Author\"}").
                contentType(MediaType.APPLICATION_JSON_UTF8);
        mockMvc.perform(requestBuilder)
                .andExpect(status().isOk()).
                andExpect(content().json("{\"result\":\"ERROR\", \"code\":-2, \"description\":\"Bad request\"}"));

    }

    @Test
    public void test2GetAuthors() throws Exception {

        MockHttpServletRequestBuilder requestBuilder = get("/authors/load");

        mockMvc.perform(requestBuilder)
                .andExpect(status().isOk()).
                andExpect(content().json("{\"result\":\"SUCCESS\", \"items\":[{\"name\":\"Author\", \"value\":\"Author\"}]}"));
    }

    @Test
    public void test3GetSortTypes() throws Exception {

        MockHttpServletRequestBuilder requestBuilder = get("/sortTypes/load");

        mockMvc.perform(requestBuilder)
                .andExpect(status().isOk()).
                andExpect(content().json(gson.toJson(SelectItemResponse.ofSortType())));
    }

    @Test
    public void test4GetQuotes() throws Exception {
        MockHttpServletRequestBuilder requestBuilder = get("/quotes/load?page=0&maxSize=3&sortType=DATE_DESC");
        mockMvc.perform(requestBuilder)
                .andExpect(status().isOk());

        requestBuilder = get("/quotes/load?maxSize=3&sortType=DATE_DESC");
        mockMvc.perform(requestBuilder)
                .andExpect(status().isOk()).
                andExpect(content().json("{\"result\":\"ERROR\", \"code\":-2, \"description\":\"Bad request\"}"));
    }
}