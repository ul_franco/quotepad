package ru.dz.grasshopper.model;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;
import ru.dz.grasshopper.config.JpaTestConfig;
import ru.dz.grasshopper.controller.request.QuotesLoadRequest;
import ru.dz.grasshopper.controller.response.quotes.QuotesLoadResponse;
import ru.dz.grasshopper.controller.response.quotes.SelectItemResponse;
import ru.dz.grasshopper.model.entity.AuthorRepository;
import ru.dz.grasshopper.model.entity.Quote;
import ru.dz.grasshopper.model.entity.QuoteRepository;

import static org.hibernate.validator.internal.util.Contracts.assertNotNull;
import static org.junit.Assert.assertEquals;

/**
 * Created by grasshopper on 23.02.16.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {JpaTestConfig.class})
@Transactional
public class QuotesBeanTest {
    @Autowired
    private QuotesBean quotesBean;
    @Autowired
    private QuoteRepository quoteRep;
    @Autowired
    private AuthorRepository authorRep;

    @Before
    public void setUp() throws Exception {
        authorRep.deleteAll();
        quoteRep.deleteAll();
        quotesBean.addQuote("A1", "quote 1.1");
        quotesBean.addQuote("A1", "quote 1.2");
        quotesBean.addQuote("A2", "quote 2.1");
    }

    @Test
    public void testAddQuote() {
        quotesBean.addQuote("A2", "quote 2.2");
        assertEquals(4, quoteRep.count());
        assertEquals(2, authorRep.count());

        quotesBean.addQuote("A3", "quote 3.1");
        assertEquals(5, quoteRep.count());
        assertEquals(3, authorRep.count());
    }

    @Test
    public void testGetAuthors() {
        SelectItemResponse response = (SelectItemResponse) quotesBean.getAuthors();
        assertNotNull(response.getItems());
        assertEquals(2, response.getItems().size());
        assertEquals("A1", response.getItems().get(0).getName());
        assertEquals("A2", response.getItems().get(1).getName());
    }

    @Test
    public void testGetQuotes() {
        QuotesLoadRequest request = new QuotesLoadRequest();
        request.setPage(0);
        request.setMaxSize(2);
        request.setSortType(SortType.DATE_DESC);

        QuotesLoadResponse response = (QuotesLoadResponse) quotesBean.getQuotes(request);
        assertNotNull(response.getPageData());
        Page<Quote> p = response.getPageData();

        assertEquals(0, p.getNumber());
        assertEquals(2, p.getTotalPages());
        assertEquals(3, p.getTotalElements());
        assertEquals(2, p.getNumberOfElements());
        assertNotNull(p.getContent());
        assertEquals(2, p.getContent().size());

        Quote q = p.getContent().get(0);
        assertEquals("A2", q.getAuthorName());
        assertEquals("quote 2.1", q.getQuote());

        q = p.getContent().get(1);
        assertEquals("A1", q.getAuthorName());
        assertEquals("quote 1.2", q.getQuote());
    }
}