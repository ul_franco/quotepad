package ru.dz.grasshopper.model.entity;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by grasshopper on 14.02.16.
 */
public class QuoteTest {

    @Test
    public void testGetAuthorName() {
        Quote q = new Quote();
        Author a = new Author();
        a.setName("aName");
        q.setAuthor(a);

        assertEquals("aName", q.getAuthorName());

        q.setAuthor(null);
        assertEquals("", q.getAuthorName());
    }
}