package ru.dz.grasshopper;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import ru.dz.grasshopper.config.JPAConfig;

@SpringBootApplication
public class QuotePadApplication {

    public static void main(String[] args) {
        SpringApplication.run(new Class<?>[]{QuotePadApplication.class, JPAConfig.class}, args);
    }
}
