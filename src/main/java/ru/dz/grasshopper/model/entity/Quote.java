package ru.dz.grasshopper.model.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by grasshopper on 13.02.16.
 */
@Entity
public class Quote implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String quote;
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    @Column(updatable = false, nullable = false)
    private Date postDate;
    @ManyToOne(cascade = CascadeType.ALL)
    @JsonBackReference
    private Author author;

    public Quote() {
        this.postDate = new Date();
    }

    public Quote(String quote) {
        this();
        this.quote = quote;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getQuote() {
        return quote;
    }

    public void setQuote(String quote) {
        this.quote = quote;
    }

    public Date getPostDate() {
        return postDate;
    }

    public void setPostDate(Date postDate) {
        this.postDate = postDate;
    }

    public Author getAuthor() {
        return author;
    }

    public void setAuthor(Author author) {
        this.author = author;
    }

    public String getAuthorName() {
        return author == null ? "" : author.getName();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Quote)) return false;

        Quote quote1 = (Quote) o;

        if (id != null ? !id.equals(quote1.id) : quote1.id != null) return false;
        if (quote != null ? !quote.equals(quote1.quote) : quote1.quote != null) return false;
        if (postDate != null ? !postDate.equals(quote1.postDate) : quote1.postDate != null) return false;
        return !(author != null ? !author.equals(quote1.author) : quote1.author != null);

    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (quote != null ? quote.hashCode() : 0);
        result = 31 * result + (postDate != null ? postDate.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Quote{" +
                "author=" + author +
                ", id=" + id +
                ", postDate=" + postDate +
                '}';
    }
}
