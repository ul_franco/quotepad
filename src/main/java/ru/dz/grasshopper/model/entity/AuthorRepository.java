package ru.dz.grasshopper.model.entity;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by grasshopper on 14.02.16.
 */
@Transactional
public interface AuthorRepository extends PagingAndSortingRepository<Author, String> {
}
