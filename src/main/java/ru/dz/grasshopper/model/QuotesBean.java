package ru.dz.grasshopper.model;

import com.google.common.collect.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import ru.dz.grasshopper.controller.request.QuotesLoadRequest;
import ru.dz.grasshopper.controller.response.ResponseIF;
import ru.dz.grasshopper.controller.response.SuccessResponse;
import ru.dz.grasshopper.controller.response.quotes.QuotesLoadResponse;
import ru.dz.grasshopper.controller.response.quotes.SelectItemResponse;
import ru.dz.grasshopper.model.entity.Author;
import ru.dz.grasshopper.model.entity.AuthorRepository;
import ru.dz.grasshopper.model.entity.Quote;
import ru.dz.grasshopper.model.entity.QuoteRepository;

/**
 * Created by olegmakeev on 15.02.16.
 */
@Service
public class QuotesBean {
    @Autowired
    private QuoteRepository quoteRep;
    @Autowired
    private AuthorRepository authorRep;

    public ResponseIF addQuote(String author, String quote) {
        Quote q = new Quote(quote);
        Author a = authorRep.findOne(author);
        if (a == null) {
            a = new Author(author);
        }
        a.getQuotes().size();
        a.addQuote(q);
        q.setAuthor(a);
        authorRep.save(a);
        return new SuccessResponse() {
        };
    }

    public ResponseIF getAuthors() {
        Iterable<Author> it = authorRep.findAll(new Sort(Sort.Direction.ASC, "name"));
        return SelectItemResponse.of(Lists.newArrayList(it));
    }

    public ResponseIF getQuotes(QuotesLoadRequest request) {
        Page<Quote> p = quoteRep.findAll(new PageRequest(request.getPage(), request.getMaxSize(), request.getSortCondition()));
        return QuotesLoadResponse.of(p);
    }
}