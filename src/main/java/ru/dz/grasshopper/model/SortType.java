package ru.dz.grasshopper.model;

import org.springframework.data.domain.Sort;

import static org.springframework.data.domain.Sort.Direction.ASC;
import static org.springframework.data.domain.Sort.Direction.DESC;

/**
 * Created by grasshopper on 14.02.16.
 */
public enum SortType {
    DATE_DESC("postDate", "По дате", DESC),
    DATE_ASC("postDate", "По дате. Старые в начале", ASC),
    AUTHOR("author.name", "По автору", ASC);

    private final String fieldName;
    private final String displayName;
    private final Sort.Direction direction;

    SortType(String fieldName, String displayName, Sort.Direction direction) {
        this.fieldName = fieldName;
        this.displayName = displayName;
        this.direction = direction;
    }

    public String getFieldName() {
        return fieldName;
    }

    public String getDisplayName() {
        return displayName;
    }

    public Sort.Direction getDirection() {
        return direction;
    }

    public Sort getSortCondition() {
        return new Sort(direction, fieldName);
    }
}
