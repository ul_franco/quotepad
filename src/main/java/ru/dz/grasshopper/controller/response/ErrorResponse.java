package ru.dz.grasshopper.controller.response;

/**
 * Created by olegmakeev on 15.02.16.
 */
public class ErrorResponse implements ResponseIF {

    private final Integer code;
    private final String description;

    protected ErrorResponse(Integer code, String description) {
        this.code = code;
        this.description = description;
    }

    public static ErrorResponse of(ErrorCode e) {
        return new ErrorResponse(e.getCode(), e.getDescription());
    }

    public static ErrorResponse of(ErrorCode e, String description) {
        return new ErrorResponse(e.getCode(), description);
    }

    public Integer getCode() {
        return code;
    }

    public String getDescription() {
        return description;
    }

    @Override
    public Result getResult() {
        return Result.ERROR;
    }
}
