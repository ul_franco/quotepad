package ru.dz.grasshopper.controller.response.quotes;

import org.springframework.data.domain.Page;
import ru.dz.grasshopper.controller.response.SuccessResponse;
import ru.dz.grasshopper.model.entity.Quote;

/**
 * Created by grasshopper on 16.02.16.
 */
public class QuotesLoadResponse implements SuccessResponse {

    private final Page<Quote> pageData;

    protected QuotesLoadResponse(Page<Quote> pageData) {
        this.pageData = pageData;
    }

    public static QuotesLoadResponse of(Page<Quote> pageData) {
        return new QuotesLoadResponse(pageData);
    }

    public Page<Quote> getPageData() {
        return pageData;
    }
}
