package ru.dz.grasshopper.controller.response;

/**
 * Created by olegmakeev on 15.02.16.
 */
public interface SuccessResponse extends ResponseIF {
    @Override
    default Result getResult() {
        return Result.SUCCESS;
    }
}
