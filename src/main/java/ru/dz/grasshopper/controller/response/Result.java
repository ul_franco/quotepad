package ru.dz.grasshopper.controller.response;

/**
 * Created by olegmakeev on 15.02.16.
 */
public enum Result {
    SUCCESS,
    ERROR
}
