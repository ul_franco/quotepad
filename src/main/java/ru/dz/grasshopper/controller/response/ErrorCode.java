package ru.dz.grasshopper.controller.response;

/**
 * Created by olegmakeev on 15.02.16.
 */
public enum ErrorCode {
    INTERNAL_ERROR(-1, "Internal server error"),
    BAD_REQUEST(-2, "Bad request");

    private final Integer code;
    private final String description;

    ErrorCode(Integer code, String description) {
        this.code = code;
        this.description = description;
    }

    public Integer getCode() {
        return code;
    }

    public String getDescription() {
        return description;
    }
}
