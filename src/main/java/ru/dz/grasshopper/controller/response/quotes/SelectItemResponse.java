package ru.dz.grasshopper.controller.response.quotes;

import ru.dz.grasshopper.controller.response.SuccessResponse;
import ru.dz.grasshopper.model.SortType;
import ru.dz.grasshopper.model.entity.Author;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by grasshopper on 19.02.16.
 */
public class SelectItemResponse implements SuccessResponse {

    private final List<SelectItem> items;

    private SelectItemResponse(List<SelectItem> items) {
        this.items = items;
    }

    public static SelectItemResponse of(List<Author> authorList) {
        return new SelectItemResponse(authorList.stream().
                map(a -> SelectItem.builder().author(a).build()).
                collect(Collectors.toList()));
    }

    public static SelectItemResponse ofSortType() {
        return new SelectItemResponse(Arrays.asList(SortType.values()).stream().
                map(s -> SelectItem.builder().sortType(s).build()).
                collect(Collectors.toList()));
    }

    public List<SelectItem> getItems() {
        return items;
    }
}
