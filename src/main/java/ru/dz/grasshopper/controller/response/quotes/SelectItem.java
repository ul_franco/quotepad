package ru.dz.grasshopper.controller.response.quotes;

import ru.dz.grasshopper.model.SortType;
import ru.dz.grasshopper.model.entity.Author;

/**
 * Created by grasshopper on 19.02.16.
 */
public class SelectItem {
    private String name;
    private String value;

    public static Builder builder() {
        return new SelectItem().new Builder();
    }

    public String getName() {
        return name;
    }

    public String getValue() {
        return value;
    }

    public class Builder {
        public Builder author(Author a) {
            SelectItem.this.name = a.getName();
            SelectItem.this.value = a.getName();
            return this;
        }

        public Builder sortType(SortType s) {
            SelectItem.this.name = s.getDisplayName();
            SelectItem.this.value = s.name();
            return this;
        }

        public SelectItem build() {
            return SelectItem.this;
        }
    }
}
