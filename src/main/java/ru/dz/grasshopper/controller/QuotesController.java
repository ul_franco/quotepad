package ru.dz.grasshopper.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import ru.dz.grasshopper.controller.request.AddQuoteRequest;
import ru.dz.grasshopper.controller.request.QuotesLoadRequest;
import ru.dz.grasshopper.controller.response.ErrorCode;
import ru.dz.grasshopper.controller.response.ErrorResponse;
import ru.dz.grasshopper.controller.response.ResponseIF;
import ru.dz.grasshopper.controller.response.quotes.SelectItemResponse;
import ru.dz.grasshopper.model.QuotesBean;

import javax.validation.Valid;

import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

/**
 * Created by grasshopper on 13.02.16.
 */
@RestController
public class QuotesController {

    @Autowired
    private QuotesBean quotesBean;

    @RequestMapping(value = "/quotes/add", method = POST)
    public ResponseIF addQuote(@Valid @RequestBody AddQuoteRequest r, BindingResult result) {
        if (result.hasErrors()) {
            return ErrorResponse.of(ErrorCode.BAD_REQUEST);
        }
        return quotesBean.addQuote(r.getAuthor(), r.getQuote());
    }

    @RequestMapping(value = "/authors/load", method = GET)
    public ResponseIF getAuthors() {
        return quotesBean.getAuthors();
    }

    @RequestMapping(value = "/sortTypes/load", method = GET)
    public ResponseIF getSortTypes() {
        return SelectItemResponse.ofSortType();
    }

    @RequestMapping(value = "/quotes/load", method = GET)
    public ResponseIF getQuotes(@Valid @ModelAttribute QuotesLoadRequest r, BindingResult result) {
        if (result.hasErrors()) {
            return ErrorResponse.of(ErrorCode.BAD_REQUEST);
        }
        return quotesBean.getQuotes(r);
    }

    @ExceptionHandler(value = {RuntimeException.class})
    public ResponseIF runtimeExceptionHandler() {
        return ErrorResponse.of(ErrorCode.INTERNAL_ERROR);
    }


    @RequestMapping(value = "/")
    public ModelAndView index() {
        return new ModelAndView("index");
    }
}
