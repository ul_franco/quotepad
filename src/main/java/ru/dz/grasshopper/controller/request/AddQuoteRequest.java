package ru.dz.grasshopper.controller.request;

import javax.validation.constraints.NotNull;

/**
 * Created by olegmakeev on 15.02.16.
 */
public class AddQuoteRequest {
    @NotNull
    private String quote;
    @NotNull
    private String author;

    public AddQuoteRequest() {
    }
    
    public String getQuote() {
        return quote;
    }

    public void setQuote(String quote) {
        this.quote = quote;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }
}
