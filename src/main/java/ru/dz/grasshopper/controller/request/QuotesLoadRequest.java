package ru.dz.grasshopper.controller.request;

import org.springframework.data.domain.Sort;
import ru.dz.grasshopper.model.SortType;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

/**
 * Created by grasshopper on 17.02.16.
 */
public class QuotesLoadRequest {
    @NotNull
    @Min(0)
    private Integer page;
    @NotNull
    @Min(1)
    private Integer maxSize;
    @NotNull
    private SortType sortType;

    public QuotesLoadRequest() {
    }

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public Integer getMaxSize() {
        return maxSize;
    }

    public void setMaxSize(Integer maxSize) {
        this.maxSize = maxSize;
    }

    public SortType getSortType() {
        return sortType;
    }

    public void setSortType(SortType sortType) {
        this.sortType = sortType;
    }

    public Sort getSortCondition() {
        return sortType.getSortCondition();
    }
}
