var QuoteBox = React.createClass({

  getInitialState: function() {
      return {
          data: [],
          totalPages:2,
          page:0,
          maxVisible:5,
          maxSize:3,
          sortType:"DATE_DESC",
          showModal:false
      }
  },

componentDidMount: function() {
  this.loadQuotes();
},

loadQuotes: function(){
  $.ajax({
      url: "http://localhost:8080/quotes/load?page="+this.state.page+"&maxSize="+this.state.maxSize+"&sortType="+this.state.sortType,
      success: this.successQuotesLoadHandler
  })
},

successQuotesLoadHandler: function(quoteResponse) {
  if (quoteResponse.result=='SUCCESS'){
    this.state.data=quoteResponse.pageData.content;
    this.state.totalPages=quoteResponse.pageData.totalPages;
    this.forceUpdate();}
    else {
      alert(quoteResponse.description);
    }
  },

  reRenderQuotes: function(event, selectedEvent) {
    this.state.page=selectedEvent.eventKey-1;
    this.loadQuotes();
  },

  sortTypeChangeHandler: function(value){
    this.state.page=0;
    this.state.sortType=value;
    this.loadQuotes();
  },

  close() {
    this.state.showModal=false;
    this.forceUpdate();
  },

  closeUpdate(){
    this.state.showModal=false;
    this.forceUpdate();
    this.loadQuotes();
  },

  open() {
    this.state.showModal=true;
    this.forceUpdate();
  },

  render: function() {

    var ButtonToolbar = ReactBootstrap.ButtonToolbar;
    var Button = ReactBootstrap.Button;
    var Modal = ReactBootstrap.Modal;
    var Grid=ReactBootstrap.Grid;
    var Row=ReactBootstrap.Row;
    var Col=ReactBootstrap.Col;
    var Pagination=ReactBootstrap.Pagination;

    return (
      <div>
        <h1 className="header">Цитатник а-ля Баш</h1>
        <br/>
        <br/>
        <Grid>
          <Row>
            <Col md={4}>
              <QuoteList ref="QuoteListRef" data={this.state.data}/>

                <Pagination
                        prev
                        next
                        first
                        last
                        ellipsis
                        boundaryLinks
                        items={this.state.totalPages}
                        maxButtons={this.state.maxVisible}
                        activePage={this.state.page+1}
                        onSelect={this.reRenderQuotes} />

            </Col>
            <Col md={4}>
              Сортировка:
              <SelectComponent
                  url="http://localhost:8080/sortTypes/load"
                  onChange={this.sortTypeChangeHandler}
                  title="Выберите тип сортировки"
              />
            <br/>
            <br/>
              <Button bsStyle="success" onClick={this.open}>+ Добавить цитату</Button>
            </Col>
          </Row>
      </Grid>

        <Modal show={this.state.showModal} onHide={this.close}>
          <Modal.Header closeButton>
          <Modal.Title>Новая цитата</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <AddForm close={this.closeUpdate}/>
          <Button onClick={this.close} bsStyle="primary">Отменить</Button>
        </Modal.Body>
        </Modal>

      </div>
    );
  }
});

var QuoteList = React.createClass({

  render: function() {
    var quoteNodes = this.props.data.map(function(quote) {
      return (
        <Quote author={quote.authorName} postDate={quote.postDate} key={quote.id}>
          {quote.quote}
        </Quote>
      );
    });

    return (
      <div>
        {quoteNodes}
        <br/>
      </div>
    );
  }
});

var Quote = React.createClass({
  render: function(){
    var date = new Date(this.props.postDate);
    var day ="0" + date.getDate();
    var month ="0" + (date.getMonth()+1);
    var year = date.getFullYear();
    var formattedDate = day.substr(-2)+'/'+month.substr(-2)+'/'+year;
  return(
      <div>
        <b>{formattedDate} &nbsp; {this.props.author} </b> <br/>
        {this.props.children} <hr/>
    </div>
    );
  }
});

var AddForm = React.createClass({

  propTypes: {
      close: React.PropTypes.func.isRequired
  },

  getInitialState: function() {
      return {
          author:'',
          quote:'',
          disabled: true,
          style: null
      }
  },
handleAuthorChange: function(e){
  this.state.author= e.target.value;
  this.forceUpdate();
  this.setState(this.validationState());
},
handleQuoteChange: function(e){
  this.state.quote= e.target.value;
  this.forceUpdate();
  this.setState(this.validationState());
},

validationState: function() {
      var authorLength = this.state.author.length;
      var quoteLength = this.state.quote.length;
      var style=(quoteLength>0 && authorLength > 0 && authorLength<=255)?'success':'warning'
      var disabled = style !== 'success';
      return { style, disabled };
    },

authorChangeHandler: function(eventKey){
  this.state.author=eventKey;
  this.forceUpdate();
  this.setState(this.validationState());
},

handleSubmit: function(){
  var request = new Object();
     request.author = this.state.author;
     request.quote = this.state.quote;
  var jsonString= JSON.stringify(request);

$.ajax({
      url: 'http://localhost:8080/quotes/add',
      type: 'post',
      dataType: 'json',
      contentType: "application/json; charset=utf-8",
      success: this.successHandler,
      error: this.errorHandler,
      data: jsonString
    });

},

successHandler: function(data){
if (data.result=="SUCCESS"){
alert("Цитата успешно добавлена");
this.props.close();
} else {
  alert(data.description);
}
},
errorHandler: function(){},

  render: function(){
    var Input=ReactBootstrap.Input;
    var ButtonInput=ReactBootstrap.ButtonInput;
    var Grid=ReactBootstrap.Grid;
    var Row=ReactBootstrap.Row;
    var Col=ReactBootstrap.Col;

    return(
      <div>
          <Grid>
              <Row>
                <Col md={3}>
                  <Input type="text"
                         placeholder="Введите имя"
                         value={this.state.author}
                         onChange={this.handleAuthorChange}
                         ref="authorRef"/>
                </Col>
                <Col md={3}>
                  <SelectComponent
                      url="http://localhost:8080/authors/load"
                      onChange={this.authorChangeHandler}
                      title="Выберите автора"
                  />
                </Col>
              </Row>
              <Row>
                <Col md={6}>
                  <Input type="textarea"
                         placeholder="Введите текст цитаты"
                         value={this.state.quote}
                         onChange={this.handleQuoteChange}
                         ref="quoteRef"/>
                </Col>
              </Row>
          </Grid>
          <ButtonInput type="button"
            value="+ Сохранить"
            bsStyle={this.state.style}
            bsSize="large"
            disabled={this.state.disabled}
            onClick={this.handleSubmit}/>

      </div>
    );
  }
});


var SelectComponent = React.createClass({
    propTypes: {
        url: React.PropTypes.string.isRequired,
        onChange: React.PropTypes.func.isRequired,
        title:React.PropTypes.string.isRequired,
    },
    getInitialState: function() {
        return {
            options: []
        }
    },
    componentDidMount: function() {
        $.ajax({
            url: this.props.url,
            success: this.successHandler
        })
    },
    successHandler: function(data) {
      var MenuItem = ReactBootstrap.MenuItem;
        var items = data.items;
        for (var i = 0; i < items.length; i++) {
            var option = items[i];
            this.state.options.push(
                <MenuItem key={i} eventKey={option.value}>{option.name}</MenuItem>
            );
        }
        this.forceUpdate();
    },
    handleChange: function(evt, value){
      this.props.onChange(value);
    },
    render: function() {
      var DropdownButton = ReactBootstrap.DropdownButton;
      var MenuItem = ReactBootstrap.MenuItem;

        return(
          <DropdownButton id={Math.random()} title={this.props.title}
            ref="selectRef" bsStyle="default"
            onSelect={this.handleChange}>{this.state.options}
          </DropdownButton>
        );
    }
});


ReactDOM.render(
  <QuoteBox/>,
  document.getElementById('content')
);
